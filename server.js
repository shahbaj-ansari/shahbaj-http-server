const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

const port = 3010;

function testUrl(url){
	if(url.length<2 || url.length>3) return false;
	if((url[1]=='html'|| url[1]=='json' || url[1]=='uuid') && url.length>2) return false;
	if(url.length == 3 && url[2] == "") return false;
	return true;
}


const server = http.createServer((req, res) => {
	console.log(`server started at port: ${port}`);
	const requestUrl = req.url.split("/");

	if (req.method === "GET" && testUrl(requestUrl)) {
		switch (requestUrl[1]) {
			case "html":
				try {
					fs.readFile("index.html", "utf-8", (err, data) => {
						if (err) {
							res.writeHead(400, { "Content-Type": "text/plain" });
							res.end(
								JSON.stringify({
									data: "Data Not Found",
								})
							);
						} else {
							res.writeHead(200, { "Content-Type": "text/html" });
							res.end(data);
						}
					});
				} catch (err) {
					console.log(err);
				}
				break;

			case "json":
				try {
					fs.readFile("data.json", "utf-8", (err, data) => {
						if (err) {
							res.writeHead(400, { "Content-Type": "application/json" });
							res.end(
								JSON.stringify({
									data: "Data Not Found",
								})
							);
						} else {
							res.writeHead(200, { "Content-Type": "application/json" });
							res.end(data);
						}
					});
				} catch (err) {
					console.log(err);
				}
				break;
			case "status":
				try {
					const reqCode = requestUrl[2];
					//console.log(reqCode);
					const statusCode = http.STATUS_CODES;

					if (statusCode[reqCode]) {
						res.writeHead(reqCode, { "Content-Type": "text/plain" });
						res.end(`You entered a status code := ${reqCode} : ${statusCode[reqCode]}`);
					} else {
						res.writeHead(400, { "Content-Type": "text/plain" });
						res.end("Wrong Response Code Entered!");
					}
				} catch (err) {
					console.log(err);
				}
				break;
			case "delay":
				try {
					const delay = requestUrl[2];
					if (delay >= 0 && delay <= 20) {
						setTimeout(() => {
							res.writeHead(200, { "Content-Type": "text/plain" });
							res.end(`Request has been completed after: ${delay} seconds`);
						}, delay * 1000);
					} else if (delay > 20 || delay < 0) {
						res.writeHead(400, { "Content-Type": "text/plain" });
						res.end(`Please enter a positive delay and not more than 20 seconds`);
					} else {
						res.writeHead(400, { "Content-Type": "text/plain" });
						res.end(`Bad Request!`);
					}
				} catch (err) {
					console.log(err);
				}
				break;
			case "uuid":
				try{
					const uuid=uuidv4();
					res.writeHead(200, { "Content-Type": "text/plain" });
					res.end(JSON.stringify({"uuid":uuid}));
				}catch(err){
					res.writeHead(400, { "Content-Type": "text/plain" });
					res.end("Error while getting uuid");
				}
				break;
			default:
				res.writeHead(404, { "Content-Type": "text/plain" });
				res.end(`Not Found!`);
		}
	} else {
		res.writeHead(404, { "Content-Type": "text/plain" });
		res.end(`Not Found!`);
	}
});

try {
	server.listen(port);
} catch (err) {
	console.log(err);
}
